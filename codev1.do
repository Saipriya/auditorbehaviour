***************************************************************************************************************************************************
*                             Once Once bitten, twice shy: Auditor behavior in the aftermath of audit failure
*Date: 29-October-2014
*Version: 1.0
***************************************************************************************************************************************************

cd "D:\IIMT backup"

***************************************************************************************************************************************************

*								STEP 1: INPUT FINANCIAL DATA 

***************************************************************************************************************************************************
insheet using firm-financial.txt
rename at assets
rename rect receivable
rename conm comp_name
rename ni net_income
drop datadate indfmt consol popsrc datafmt cusip curcd costat
sort gvkey fyear
save firm-financial-new, replace
drop if cik==.
drop if fyear==.
duplicates tag cik fyear, gen(dup)
*There are only 2 duplicates for Laidlaw international inc. These duplicates have assets==. So will drop them
drop if dup==1 & assets==.
sort cik fyear
save firm-financial-new, replace

***************************************************************************************************************************************************

*								STEP 2: INPUT AUDITOR DATA 

***************************************************************************************************************************************************
clear
insheet using auditor-name-city.txt
rename company_fkey cik
gen fyear= substr(fiscal_year_end, 6,4)
destring fyear, replace
duplicates report cik fyear auditor_fkey
duplicates drop cik fyear auditor_fkey, force

*When multiple auditors are present in the same year, will take latest auditor *
gen month= substr(fiscal_year_end, 3,3)
encode month, gen(month1)
label define month1 1 "JAN", modify
label define month1 2 "FEB", modify
label define month1 3 "MAR", modify
label define month1 4 "APR", modify
label define month1 5 "MAY", modify
label define month1 6 "JUN", modify
label define month1 7 "JUL", modify
label define month1 8 "AUG", modify
label define month1 9 "SEP", modify
label define month1 10 "OCT", modify
label define month1 11 "NOV", modify
label list

duplicates tag cik fyear, gen(dup1)
save audnamecity-new, replace
sort cik fyear month1
by cik fyear: drop if dup1==1 & _n==1
by cik fyear: drop if dup1==2 & (_n==1 | _n==2)
by cik fyear: drop if dup1==3 & ( _n==1 | _n==2 |_n==3 )
by cik fyear: drop if dup1==4 & ( _n==1 | _n==2 |_n==3| _n==4 )
save audnamecity-new, replace
***************************************************************************************************************************************************

*								STEP 3: MERGING THE 2 FILES & GENERATING DISCRETIONARY ACCRUALS
***************************************************************************************************************************************************
clear
use firm-financial-new
merge 1:1 cik fyear using audnamecity-new
keep if _merge==3
drop _merge
save merged, replace
generate cash_flow_op = oancf - xidoc
generate total_accrual = ibc - cash_flow_op
sort cik fyear
by cik: generate asset_prev = assets[_n-1]
generate TA_deflated = total_accrual/asset_prev
by cik: generate del_rev = sale[_n] - sale[_n-1]
by cik: generate del_rev_deflated = del_rev/asset_prev
by cik: generate inv_assets = 1/asset_prev
by cik: generate PPE_deflated = ppegt/asset_prev
by cik: generate del_receivable = receivable[_n] - receivable[_n-1]
by cik: generate del_k_deflated = (del_rev- del_receivable)/asset_prev

nsplit sic, digits(2 2) generate(sic_ind useless)
drop useless
drop if sic_ind ==60 |sic_ind==61|sic_ind ==62 |sic_ind ==63 |sic_ind ==64
save merged, replace

clear
use merged
generate use=1
replace use=0 if missing(TA_deflated)|missing(PPE_deflated)|missing(del_rev_deflated)|missing(inv_assets)
sort sic_ind fyear

foreach i of numlist 1995/2013{
	by sic_ind: egen usetot`i' = total(use)if fyear==`i'
}
*
generate usetot = 0
foreach i of numlist 1995/2013{
	replace usetot = usetot`i' if fyear == `i'
}
*

foreach i of numlist 1995/2013{
	drop usetot`i'
}
*
drop if usetot<8
save merged, replace
pctile pct=TA_deflated, nq(100)
drop if TA_deflated<r(r1)
drop if TA_deflated>r(r99)
sum TA_deflated, detail
drop pct
save merged, replace

sort sic_ind fyear

foreach i of numlist 1999/2013{
clear
use merged 
parmby "regress TA_deflated inv_assets PPE_deflated del_rev_deflated if fyear==`i', noconstant", by(sic_ind) saving(result`i',replace)
}
*
foreach i of numlist 1999/2013{
clear
use result`i'
drop stderr dof t p min95 max95
sort sic_ind
by sic_ind: generate assetinv1 = estimate if _n==1
by sic_ind: generate defppe1 = estimate if _n==2
by sic_ind: generate defrev1 = estimate if _n==3

by sic_ind: egen assetinv`i' = total(assetinv1)
by sic_ind: egen defppe`i' = total(defppe1)
by sic_ind: egen defrev`i' = total(defrev1)

drop assetinv1 defppe1 defrev1 
drop parmseq parm estimate
duplicates drop
save result`i', replace
}

clear
use merged
sort sic_ind
save merged, replace
clear

foreach i of numlist 1999/2012{
clear
use merged
merge m:1 sic_ind using result`i'
drop _merge
save merged,replace
}
**********************************************************************************************************
generate NA =.
foreach i of numlist 1999/2012{
replace NA = assetinv`i'*inv_assets + defppe`i'*PPE_deflated + defrev`i'*del_k_deflated if fyear==`i'
save merged,replace
} 

drop assetinv* defppe* defrev*
generate DA= TA_deflated - NA
generate absDA = abs(DA)
save merged,replace

pctile pct1 = DA , nq(100)
drop if DA<r(r1)
drop if DA>r(r99)
sum DA, detail
save merged,replace


***************************************************************************************************************************************************

*								STEP 4: GENERATING AFTER DUMMY AT CITY, BRANCH, AUDIT FIRM AND AUDIT PROFESSION AS A WHOLE LEVEL

***************************************************************************************************************************************************

clear
use merge
save mergetemp
clear
use mergetemp
sort cik fyear
gen num=_n
save mergetemp, replace

insheet using "fraud_branch_year.csv"
save fraud_branch_city

merge 1:1  num using fraud_branch_city
drop _merge
save mergetemp, replace

replace auditor_city=lower(auditor_city)
replace city = lower(city)
replace auditor_city_fraud = lower(auditor_city_fraud)
replace bus_city_fraud = lower(bus_city_fraud)
save mergetemp, replacee

gen samebranch=0
foreach i of numlist 1/17{
replace samebranch=1 if auditor_fkey==auditor_fraud_key[`i'] & auditor_city==auditor_city_fraud[`i'] 
}

gen branchafter=0
foreach i of numlist 1/17{
replace branchafter=1 if auditor_fkey==auditor_fraud_key[`i'] & auditor_city==auditor_city_fraud[`i'] & fyear >=year_investigate[`i']
}
*
gen samecity=0
foreach i of numlist 1/17{
replace samecity=1 if city==bus_city_fraud[`i'] 
}

gen cityafter=0
foreach i of numlist 1/17{
replace cityafter=1 if city==bus_city_fraud[`i'] & fyear >=year_investigate[`i']
}

save mergetemp, replace

***************************************************************************************************************************************************

*								STEP 5: GENERATING OTHER CONTROLS

***************************************************************************************************************************************************
*Calculating ROA and ROA+
sort cik fyear
by cik: gen roa = net_income/asset_prev 
by cik: gen roa_prev = roa[_n-1]
by cik: gen roa_prev_plus = 0
by cik: replace roa_prev_plus =roa_prev if roa_prev>=0

*Generating total accruals previous year
sort cik fyear
by cik: gen acc_prev = TA_deflated[_n-1] 
gen acc_prev_plus =0
replace acc_prev_plus = acc_prev if acc_prev >=0

* Calculating tenure of auditors* 
sort cik fyear
generate tenure = 1
replace tenure=0 if auditor_fkey==.
by cik: replace tenure = tenure[_n-1] +1 if auditor_fkey[_n]==auditor_fkey[_n-1]

*Cash flow from operations
generate cfo_deflated = cash_flow_op/ asset_prev
gen cfo_plus = 0
replace cfo_plus = cfo_deflated if cfo_deflated>=0

*Genrate log of assets
gen log_asset = log(assets)

save mergetemp, replace

sort num
foreach i of numlist 1/17{
drop if cik == cik_fraud[`i']
}
***************************************************************************************************************************************************

*								STEP 6: REGRESSION

***************************************************************************************************************************************************
reg absDA samecity cityafter samebranch branchafter tenure log_asset acc_prev_plus acc_prev roa_prev_plus roa_prev cfo_deflated cfo_plus i.sic_ind i.fyear,robust 

***************************************************************************************************************************************************

*								STEP 6: REGRESSION
* Dropping except for cendant, Xerox, riteaid and mckesson as they don't have pre period values

***************************************************************************************************************************************************
clear
use mergetemp
save mergetemp_excl

 
local excl 2 5 9 15
foreach i of local excl {
replace samebranch=0 if auditor_fkey==auditor_fraud_key[`i'] & auditor_city==auditor_city_fraud[`i'] 
}

foreach i of local excl {
replace branchafter=0 if auditor_fkey==auditor_fraud_key[`i'] & auditor_city==auditor_city_fraud[`i'] & fyear >=year_investigate[`i']
}
*
foreach i of local excl {
replace samecity=0 if city==bus_city_fraud[`i'] 
}

foreach i of local excl {
replace cityafter=0 if city==bus_city_fraud[`i'] & fyear >=year_investigate[`i']
}
save mergetemp_excl, replace
*
reg absDA samecity cityafter samebranch branchafter tenure log_asset acc_prev_plus acc_prev roa_prev_plus roa_prev cfo_deflated cfo_plus i.sic_ind i.fyear,robust 
save mergetemp_excl, replace

outreg using results, title("Regression of Absolute Abnormal Accruals on branch after and city after dummies") starlevels(10 5 1)
rvfplot

***************************************************************************************************************************************************

*								STEP 7: IS THE IMPROVEMENT IN IMPLICATED BRANCH > REST OF THE WORST BRANCHES OF AUDIT FIRM
* Dropping except for cendant, Xerox, riteaid and mckesson as they don't have pre period values

***************************************************************************************************************************************************
* Need to generate avg discretionary accruals of each individual brnach per year
clear
use mergetemp_excl
keep cik fyear auditor_fkey auditor_city cik_fraud auditor_name year_investigate auditor_fraud auditor_fraud_key auditor_city_fraud absDA
save worstbranches, replace

sort auditor_fkey auditor_city fyear
by auditor_fkey auditor_city fyear: egen avg_absDA = mean(absDA)
keep auditor_city auditor_fkey fyear avg_absDA
duplicates drop auditor_city auditor_fkey fyear avg_absDA, force
gen num=_n
save worstbranches, replace

merge 1:1  num using fraud_branch_city
drop _merge
save worstbranches, replace

local incl 1 3 4 6 7 8 10 11 12 13 14 16 17
gen worstbranch=0
gen fraud_event_year = 0
foreach i of local incl{
sort num
*sum avg_absDA if auditor_fkey==auditor_fraud_key[`i'] & fyear == year_investigate[`i'], detail
*
pctile pct=avg_absDA if auditor_fkey==auditor_fraud_key[`i'] & fyear == year_investigate[`i'], nq(100)
scalar ninetypct = r(r96)
disp ninetypct
drop pct
replace worstbranch=1 if auditor_fkey==auditor_fraud_key[`i'] & avg_absDA >= ninetypct & fyear== year_investigat[`i']
}
*
keep if worstbranch==1
keep auditor_fkey auditor_city worstbranch fyear
rename fyear fraud_event_year
rename auditor_fkey worst_audkey
rename auditor_city worst_audcity
drop if worst_audcity==""
gen num = _n
save worstbranches, replace

clear
use mergetemp_excl
sort num
merge 1:1 num using worstbranches
drop _merge worstbranch

gen worstbranch=0
gen worstbranch_after=0
foreach i of numlist 1/141{
replace worstbranch=1 if auditor_fkey ==worst_audkey[`i'] & auditor_city==worst_audcity[`i']
replace worstbranch_after=1 if auditor_fkey==worst_audkey[`i'] & auditor_city==worst_audcity[`i'] & worstbranch==1 & fyear>= fraud_event_year[`i']
}
save mergetemp_worst, replace
reg absDA samecity cityafter samebranch branchafter worstbranch worstbranch_after tenure log_asset acc_prev_plus acc_prev roa_prev_plus roa_prev cfo_deflated cfo_plus i.sic_ind i.fyear,robust 
***************************************************************************************************************************************************

*								STEP 8: IS THE IMPROVEMENT IN SAME INDUSTRY> REST OF THE WORLD
* Dropping except for cendant, Xerox, riteaid and mckesson as they don't have pre period values

***************************************************************************************************************************************************
